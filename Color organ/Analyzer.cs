﻿//#define DEBUG

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;
using System.Threading;
using Un4seen.Bass;
using Un4seen.BassWasapi;
using System.IO.Ports;

namespace Color_organ
{
    class Analyzer
    {
        private const int PERIOD = 500;
        private const int FREQUENCY = 50;
        private const int STEPS = (PERIOD * FREQUENCY) / 1000;

        private ComboBox comboDevice;
        private ComboBox comboPort;

        private Thread thread;
        private static SerialPort sp;

        private static bool enabled;
        private static int selectedDevice;

        private float[] fft;
        private WASAPIPROC process;

        private MainWindow window;


        public Analyzer(MainWindow window, ComboBox comboDevice, ComboBox comboPort)
        {
            this.comboDevice = comboDevice;
            this.comboPort = comboPort;
            this.window = window;

            sp = new SerialPort();
            sp.WriteTimeout = 5000;

            fft = new float[1024];
            process = new WASAPIPROC(Process);
            
            init();
        }


        private void init()
        {
            BassNet.Registration("jan@jstanek.cz", "2X1931414192839");

            comboDevice.Items.Clear();
            int count = BassWasapi.BASS_WASAPI_GetDeviceCount();
            for (int i = 0; i < count; i++)
            {
                var device = BassWasapi.BASS_WASAPI_GetDeviceInfo(i);
                if (device.IsEnabled && device.IsLoopback)
                    comboDevice.Items.Add(i + "-" + device.name);
            }

            Bass.BASS_SetConfig(BASSConfig.BASS_CONFIG_UPDATETHREADS, false);
            Bass.BASS_Init(0, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);
        }


        public void start()
        {
            enabled = true;

            selectedDevice = Int32.Parse(comboDevice.SelectedValue.ToString().Split('-')[0]);

            BassWasapi.BASS_WASAPI_Init(selectedDevice, 0, 0, BASSWASAPIInit.BASS_WASAPI_BUFFER, 1f, 0.05f, process, IntPtr.Zero);
            BassWasapi.BASS_WASAPI_Start();

            sp.PortName = comboPort.SelectedItem.ToString();
            sp.BaudRate = 115200;
            
            thread = new Thread(background);
            thread.Start();

            window.setButtonsStart();
        }

        public void stop()
        {
            enabled = false;
            BassWasapi.BASS_WASAPI_Free();

            window.setButtonsStop();
        }

        private void background() {
            try {
                if (!sp.IsOpen)
                    sp.Open();
            } catch (Exception e)
            { }

            double[] act = new double[3];
            double[] prev = new double[3];
            double[] next = new double[3];
            byte[] buffer = new byte[4];
            buffer[0] = 0;

            for (int i = 0; i < 3; i++)
            {
                act[i] = 0;
                prev[i] = 0;
            }

            try
            {
                while (enabled)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        next[i] = 0;
                    }

                    for (int i = 0; i < STEPS; i++)
                    {
                        if (i % 5 == 0)
                        {
                            BassWasapi.BASS_WASAPI_GetData(fft, (int)(BASSData.BASS_DATA_FFT2048 | BASSData.BASS_DATA_FFT_REMOVEDC));

                            float[] vals = new float[3];

                            computeBands(3, fft, vals);

                            for (int j = 0; j < 3; j++)
                            {
                                next[j] += vals[j];
                            }
                        }

                        for (int j = 0; j < 3; j++)
                        {
                            double diff = act[j] - prev[j];
                            byte res = (byte)(prev[j] + (diff * i) / STEPS);
                            if (res < 1)
                                buffer[j + 1] = 1;
                            else
                                buffer[j + 1] = res;
                        }

                        sp.Write(buffer, 0, 4);
      
                        #if DEBUG
                        Console.WriteLine(buffer[1] + " " + buffer[2] + " " + buffer[3]);
                        #endif

                        Thread.Sleep(PERIOD / STEPS);
                    }

                    double total = 0;

                    for (int i = 0; i < 3; i++)
                    {
                        prev[i] = act[i];

                        next[i] *= 200;
                        next[i] = Math.Pow(next[i], 0.75);

                        if (next[i] < 1)
                            act[i] = 1;
                        else
                            act[i] = next[i];

                        total += act[i];
                    }

                    for (int i = 0; i < 3; i++)
                    {
                        act[i] = (act[i] * 255) / total;
                    }

                    #if DEBUG
                    Console.WriteLine("--------------------------");
                    #endif
                }
            }
            catch (Exception e) { }
            finally
            {
                sp.Close();
            }
        }

        private void computeBands(int bands, float[] fft, float[] avgs)
        {
            double b = 0;
            //for (int i = 1; i < 13; i++)
            for (int i = 0; i < 7; i++)
                b += fft[i];
            avgs[0] = (float)(b);

            double g = 0;
            //for (int i = 13; i < 98; i++)
            for (int i = 7; i < 46; i++)
                g += fft[i];
            avgs[1] = (float)(g);

            double r = 0;
            //for (int i = 98; i < 779; i++)
            for (int i = 64; i < 358; i++)
                r += fft[i];
            avgs[2] = (float)(r);
        }

        private float getBandWidth()
        {
            return (float)44100 / (float)2048;
        }

        private int freqToIndex(int freq)
        {
            if (freq < getBandWidth() / 2) return 0;
            if (freq > 44100 / 2 - getBandWidth() / 2) return 512;
            float fraction = (float)freq / (float)44100;
            int i = (int)(2048 * fraction);
            return i;
        }

        private int Process(IntPtr buffer, int length, IntPtr user)
        {
            return length;
        }
    }
}
