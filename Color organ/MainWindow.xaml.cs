﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Un4seen.Bass;
using Un4seen.BassWasapi;
using System.IO.Ports;



namespace Color_organ
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Analyzer analyzer;

        public MainWindow()
        {
            InitializeComponent();
            analyzer = new Analyzer(this, comboDevice, comboPort);
            refreshPorts();

            loadSettings();
        }

        public void setButtonsStop()
        {
            comboPort.IsEnabled = true;
            comboDevice.IsEnabled = true;
            btnStart.IsEnabled = true;
            btnStop.IsEnabled = false;
        }

        public void setButtonsStart()
        {
            comboPort.IsEnabled = false;
            comboDevice.IsEnabled = false;
            btnStart.IsEnabled = false;
            btnStop.IsEnabled = true;
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
           
            analyzer.start();
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            
            analyzer.stop();
        }

        private void comboPort_DropDownOpened(object sender, EventArgs e)
        {
            refreshPorts();
        }

        private void refreshPorts()
        {
            comboPort.Items.Clear();
            var ports = SerialPort.GetPortNames();
            foreach (var port in ports)
                comboPort.Items.Add(port);

            string selectedPort = Properties.Settings.Default.port;
            if (comboPort.Items.Contains(selectedPort))
                comboPort.SelectedItem = selectedPort;
        }

        private void loadSettings()
        {
            if (Properties.Settings.Default.upgrade)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.upgrade = false;
                saveSettings();
            }

            string device = Properties.Settings.Default.device;
            if (comboDevice.Items.Contains(device))
                comboDevice.SelectedItem = device;

            string port = Properties.Settings.Default.port;
            if (comboPort.Items.Contains(port))
                comboPort.SelectedItem = port;
        }

        private void saveSettings()
        {
            Properties.Settings.Default.Save();
        }

        private void comboDevice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Properties.Settings.Default.device = comboDevice.SelectedItem.ToString();
            saveSettings();
        }

        private void comboPort_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboPort.HasItems)
            {
                Properties.Settings.Default.port = comboPort.SelectedItem.ToString();
                saveSettings();
            }
        }

        private void mainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            analyzer.stop();
        }
    }
}
